const en = {
	start: "Start",
	language: "English",
	pause: "Paused Game",
	continue: "Continue",
	score: "Score",
	bestScore: "Best Score",
	selectLevel: "Select the Difficulty",
	easy: "Easy",
	normal: "Normal",
	hard: "Hard",
	back: "Back",
	gameOver: "Game Over",
	playAgain: "Play Again",
	settings: "Settings",
	fullScreen: "Full Screen",
	sound: "Sound",
	leaderboard: "Leaderboard",
	noLeaderboard: "No score register\nYet..",
};

export default en;
