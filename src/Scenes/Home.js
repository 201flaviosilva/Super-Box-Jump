import Configs from "../Config/Configs";
import Settings from "../State/Settings";

import EventManager from "../State/EventManager";
import { TextStyle } from "../Theme";
import { randomColor } from "../Utils/Utils";

import TextButton from "../Components/TextButton";

import Player from "../Objects/Player";
import Platforms from "../Objects/Platforms";

export default class Home extends Phaser.Scene {
	constructor() {
		super({ key: "Home", });
	}

	init(data) {
		this.Settings = new Settings();
		this.isChangeLanguage = data.isChangeLanguage ?? false;
	}

	create() {
		const { width, height, middleWidth, middleHeight } = Configs.screen;

		this.createMiniGame();

		{ // Title & SubTitle
			const title = this.add.text(middleWidth, 50, "Super", TextStyle.home.title).setOrigin(0.5);
			const subTitle = this.add.text(middleWidth, title.y + 50, "Box Jump", TextStyle.home.subTitle).setOrigin(0.5);

			this.time.addEvent({
				delay: 5 * 1000, // 5 seconds
				callback: () => title.setColor(randomColor()),
				loop: true,
			});

			this.tweens.add({
				targets: subTitle,
				scale: { from: 4, to: 1 },
				duration: 2500,
				ease: "Back.easeIn",
				paused: this.isChangeLanguage,
			});
		}

		{ // Start Button
			this.startBTN = new TextButton(this, middleWidth, middleHeight - 50, this.Settings.output.start, TextStyle.home.menu,
				() => {
					this.scene.launch("SelectLevel");
					this.removeButtonsInteractive();
				});
		}

		{ // Leaderboard Button
			this.leaderboardBTN = new TextButton(this, middleWidth, middleHeight, this.Settings.output.leaderboard, TextStyle.home.menu,
				() => {
					this.scene.launch("Leaderboard");
					this.removeButtonsInteractive();
				});
		}

		{ // Settings Button
			this.settingsBTN = new TextButton(this, middleWidth, middleHeight + 50, this.Settings.output.settings, TextStyle.home.menu,
				() => {
					this.scene.launch("GameSettings");
					this.removeButtonsInteractive();
				});
		}

		EventManager.on("LanguageChanged", () => { this.scene.restart({ isChangeLanguage: true }); }, this);
		EventManager.on("ReactivateHomeButtons", this.addButtonsInteractive, this);

		if (this.isChangeLanguage) this.removeButtonsInteractive();
	}

	removeButtonsInteractive() {
		this.startBTN.removeInteractive();
		this.leaderboardBTN.removeInteractive();
		this.settingsBTN.removeInteractive();
	}

	addButtonsInteractive() {
		this.startBTN.setInteractive({ useHandCursor: true, });
		this.leaderboardBTN.setInteractive({ useHandCursor: true, });
		this.settingsBTN.setInteractive({ useHandCursor: true, });
	}


	createMiniGame() {
		const { width, height, middleWidth, middleHeight } = Configs.screen;

		const platformGroup = new Platforms(this.physics.world, this).setAlpha(0.5);

		const playersGroup = this.physics.add.group({
			classType: Player,
			collideWorldBounds: true,
			runChildUpdate: true,
		});
		const player = playersGroup.get(middleWidth, middleHeight + 100);

		this.physics.add.collider(player, platformGroup);
	}
}
