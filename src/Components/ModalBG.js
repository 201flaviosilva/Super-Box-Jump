import Configs from "../Config/Configs";
import Settings from "../State/Settings";

import { TextStyle } from "../Theme";

export default class ModalBG extends Phaser.GameObjects.Graphics {
	constructor(scene, options) {
		super(scene, options);
		scene.add.existing(this);

		this.Settings = new Settings();

		const { width, height, middleWidth, middleHeight } = Configs.screen;

		this.setDefaultStyles({
			x: 0,
			y: 0,

			lineStyle: {
				width: 5,
				color: 0xffff00,
				alpha: 1,
			},
			fillStyle: {
				color: 0x000000,
				alpha: 0.75,
			},

			add: true,
		});

		const padding = 25;
		const rect = {
			x: padding,
			y: padding,
			width: width - padding * 2,
			height: height - padding * 2,
			radius: 10,
		};

		this.clear();
		this.fillRoundedRect(rect.x, rect.y, rect.width, rect.height, rect.radius);
		this.strokeRoundedRect(rect.x, rect.y, rect.width, rect.height, rect.radius);
	}

	addTitle(name) {
		const { width, height, middleWidth, middleHeight } = Configs.screen;
		this.title = this.scene.add.text(middleWidth, 75, this.Settings.output[name], TextStyle[name].title).setOrigin(0.5);
	}
}
