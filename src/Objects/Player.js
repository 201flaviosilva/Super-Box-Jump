import Configs from "../Config/Configs";

export default class Player extends Phaser.Physics.Arcade.Sprite {
	constructor(scene, x, y) {
		super(scene, x, y, "Player");

		this.alive = true;
		this.speed = 200;
		this.jumpForce = 350;

		this.setDepth(10);

		this.particles = this.scene.add.particles("Player")
			.createEmitter({
				x: 0,
				y: 0,
				frequency: -1,
				quantity: 5,
				collideBottom: true,
				collideTop: true,
				collideLeft: true,
				collideRight: true,
				scale: { start: 0.2, end: 0 },
				alpha: { start: 0.5, end: 0 },
				speed: { min: 50, max: 150 },
				lifespan: 500,
				gravityY: 300,
			});

		this.particlesPoint = this.scene.add.particles("Player")
			.createEmitter({
				x: -100,
				y: -100,
				frequency: -1,
				quantity: 100,
				scale: { start: 0.5, end: 0 },
				alpha: { start: 0.5, end: 0 },
				speed: { min: 50, max: 150 },
				lifespan: 1000,
				gravityY: 0,
				on: true,
			});

		this.particlesDead = this.scene.add.particles("Dead")
			.createEmitter({
				x: 0,
				y: 0,
				frequency: -1,
				quantity: 250,
				scale: { start: 0.5, end: 0 },
				alpha: { start: 0.5, end: 0 },
				speed: { min: 50, max: 250 },
				lifespan: Configs.timers.restart,
			});

		const keyboardControllers = Configs.controllers.keyboard;
		this.keys = this.scene.input.keyboard.addKeys(keyboardControllers);

		this.gamepadIndex = Configs.controllers.gamePad;


		this.createGamepad();
		if (!this.scene.sys.game.device.os.desktop) this.createButtons();
	}

	createGamepad() {
		this.scene.input.gamepad.on("down", (gamepad, button, value) => {
			const btnIndex = button.index;
			if (this.gamepadIndex.left === btnIndex) this.keys.left1.isDown = true;
			if (this.gamepadIndex.right === btnIndex) this.keys.right1.isDown = true;
			if (this.gamepadIndex.down === btnIndex) this.keys.down1.isDown = true;
			if (this.gamepadIndex.up1 === btnIndex ||
				this.gamepadIndex.up2 === btnIndex ||
				this.gamepadIndex.up3 === btnIndex ||
				this.gamepadIndex.up4 === btnIndex ||
				this.gamepadIndex.up5 === btnIndex) this.keys.up1.isDown = true;
		});

		this.scene.input.gamepad.on("up", (gamepad, button, value) => {
			const btnIndex = button.index;
			if (this.gamepadIndex.left === btnIndex) this.keys.left1.isDown = false;
			if (this.gamepadIndex.right === btnIndex) this.keys.right1.isDown = false;
			if (this.gamepadIndex.down === btnIndex) this.keys.down1.isDown = false;
			if (this.gamepadIndex.up1 === btnIndex ||
				this.gamepadIndex.up2 === btnIndex ||
				this.gamepadIndex.up3 === btnIndex ||
				this.gamepadIndex.up4 === btnIndex ||
				this.gamepadIndex.up5 === btnIndex) this.keys.up1.isDown = false;
		});
	}

	createButtons() {
		const { width, height, middleWidth, middleHeight } = Configs.screen;

		this.scene.input.addPointer(2);

		const y = height - 100;
		const scale = 0.1;
		const alpha = 0.25;

		// Left
		this.scene.add.sprite(100, y, "Arrow")
			.setScale(scale)
			.setAlpha(alpha)
			.setFlipX(true).setInteractive({ cursor: "pointer", })
			.on("pointerdown", () => this.keys.left1.isDown = true)
			.on("pointerup", () => this.keys.left1.isDown = false);

		// Right
		this.scene.add.sprite(250, y, "Arrow")
			.setScale(scale)
			.setAlpha(alpha)
			.setInteractive({ cursor: "pointer", })
			.on("pointerdown", () => this.keys.right1.isDown = true)
			.on("pointerup", () => this.keys.right1.isDown = false);

		// Jump
		this.scene.add.sprite(width - 100, y, "Arrow")
			.setScale(scale)
			.setAlpha(alpha)
			.setAngle(-90)
			.setInteractive({ cursor: "pointer", })
			.on("pointerdown", () => this.keys.up1.isDown = true)
			.on("pointerup", () => this.keys.up1.isDown = false);
	}

	explodePoint() {
		const { x, y, alive, scene } = this;
		if (!alive || !scene) return;

		this.particlesPoint.setPosition(x, y);
		this.particlesPoint.explode();
	}

	activateLights() {
		this.setPipeline("Light2D");
		this.light = this.scene.lights.addLight(this.x, this.y, 100).setIntensity(6);
	}

	kill() {
		this.alive = false;
		this.particlesDead.setPosition(this.x, this.y);
		this.particlesDead.explode();

		this.scene.tweens.add({
			targets: this,
			ease: "Bounce.easeInOut",
			duration: Configs.timers.restart,
			scale: { from: 1, to: 0.05, },
			alpha: { from: 1, to: 0.05, },
		});
	}

	update() {
		const { x, y, height, keys, alive, scene, light } = this;
		if (!alive || !scene) return;

		if (keys.left1.isDown || keys.left2.isDown) this.setVelocityX(-this.speed);
		else if (keys.right1.isDown || keys.right2.isDown) this.setVelocityX(this.speed);
		else this.setVelocityX(0);

		if ((keys.up1.isDown || keys.up2.isDown || keys.up3.isDown) && (this.body.onFloor() || this.body.onWall())) this.setVelocityY(-this.jumpForce);
		else if ((keys.down1.isDown || keys.down2.isDown) && this.body.velocity.y < 0) this.setVelocityY(0);

		if (this.body.onCeiling()) this.emitParticles(x, y - height / 2, 5);
		else if (this.body.onFloor() && this.body.velocity.x !== 0) this.emitParticles(x, y + height / 2);
		else if (this.body.onWall() && this.body.velocity.y !== 0) this.emitParticles(x, y);

		light?.setPosition(x, y);

		// console.log(this.gamepads);
	}

	emitParticles(x, y, times = 1) {
		for (let i = 0; i < times; i++) {
			this.particles.setPosition(x, y);
			this.particles.explode();
		}
	}
}

export class PlayerGroup extends Phaser.Physics.Arcade.Group {
	constructor(world, scene) {
		const config = {
			classType: Player,
			collideWorldBounds: true,
			runChildUpdate: true,
		};
		super(world, scene, config);

		const { width, height, middleWidth, middleHeight } = Configs.screen;

		this.get(middleWidth, middleHeight + 50);
	}

	getCurrentPlayer() {
		return this.getChildren()[0];
	}
}
