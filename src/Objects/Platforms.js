import Configs from "../Config/Configs";
export default class Platforms extends Phaser.Physics.Arcade.StaticGroup {
	constructor(world, scene) {
		super(world, scene);
		const { width, height, middleWidth, middleHeight } = Configs.screen;

		const texture = "Platform";

		// Box (Bounds)
		this.create(middleWidth, 0, texture).setScale(40, 0.5).refreshBody();
		this.create(middleWidth, height, texture).setScale(40, 0.5).refreshBody();
		this.create(0, middleHeight, texture).setScale(0.5, 22).refreshBody();
		this.create(width, middleHeight, texture).setScale(0.5, 22).refreshBody();

		// Center
		this.create(middleWidth, 150, texture).setScale(16, 1).refreshBody();
		this.create(middleWidth, middleHeight, texture).setScale(8, 1).refreshBody();
		this.create(middleWidth, height - 150, texture).setScale(24, 1).refreshBody();

		// Laterals
		this.create(200, middleHeight - 100, texture).setScale(6, 1).refreshBody();
		this.create(width - 200, middleHeight - 100, texture).setScale(6, 1).refreshBody();
		this.create(200, middleHeight + 100, texture).setScale(6, 1).refreshBody();
		this.create(width - 200, middleHeight + 100, texture).setScale(6, 1).refreshBody();
	}

	activateLights() {
		this.children.each(child => {
			child.setPipeline("Light2D");
		});
	}
}

